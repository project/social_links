# Social Links API

Light-weight social share module that provides pop up links that can be added to any fieldable entity.

Clicking on the link displays a pop up in which to share your page's content.

Default services provided:
 - twitter
 - facebook
 - email

Alternatively, you can provide callback overrides for the default services or add new service providers altogether.

### Features
 - Reusable API
 - Support for ALL entity types

# Altering and extending

### Hook usage
```php
function MYMODULE_social_links_alter($links) {
  // Remove un wanted links.
  unset($links['email']);

  // Set SVGs to use.
  $links['facebook']['svg'] = 'social-link-facebook';
  $links['twitter']['svg'] = 'social-link-twitter';

  // Add LinkedIn provider using included pop up class.
  $links['linkedin'] = [
    'path' => 'https://www.linkedin.com/sharing/share-offsite/?url=%s&title=%s',
    'class' => '\Drupal\social_links\SocialLinkPopUp',
  ];

  return $links;
}
```
### Default configuration usage and behaviour
 - `path` (required) Uses `sprintf()` to insert the `url` and the `title` (in that order) into the given url. e.g `https://example.com?url=%s&title=%s`.
 - `class` (optional) When omitted the base `\Drupal\social_links\SocialLink` class will be used. For custom classes see below.
 - `svg` (optional) If set, it will take the place of the title as the body of the link.
 
### Custom provider handler classes
You can customize handler behaviour by specifying a class in the provider config.
```php
  $links['...'] = [
    '...'
    'class' => '\Drupal\my_module\MyCustomSocialLinkClass',
  ];
```
Your class should either extend the `SocialLink` base class (see `SocialLinkPopup` for example) or
implement the `SocialLinkInterface`.

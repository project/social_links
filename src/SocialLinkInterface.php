<?php

namespace Drupal\social_links;

use Drupal\Core\Entity\EntityInterface;

interface SocialLinkInterface {

  /**
   * Render link for a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to render the links for.
   *
   * @return \Drupal\Core\Link
   *   Link object.
   */
  public function render(EntityInterface $entity);
}

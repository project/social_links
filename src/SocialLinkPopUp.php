<?php

namespace Drupal\social_links;

use Drupal\Core\Entity\EntityInterface;

class SocialLinkPopUp extends SocialLink implements SocialLinkInterface {

  /**
   * {@inheritDoc}
   */
  public function alterOptions($linkOptions, EntityInterface $entity) {
    $linkOptions['attributes']['class'][] = 'social-link-popup';

    return $linkOptions;
  }
}

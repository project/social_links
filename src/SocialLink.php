<?php

namespace Drupal\social_links;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

class SocialLink implements SocialLinkInterface{

  /**
   * @var string
   */
  protected $name;

  /**
   * @var array
   */
  protected $config;

  public function __construct(string $name, array $config) {
    if (!array_key_exists('path', $config)) {
      throw new \Exception('Social Links: "' . $name . ' provider" path is required.');
    }

    $this->name = $name;
    $this->config = $config;
  }

  /**
   * Render link for a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to render the links for.
   *
   * @return \Drupal\Core\Link
   *   Link object.
   */
  public function render(EntityInterface $entity) {
    $linkOptions = $this->buildLinkOptions();
    $linkPath = $this->buildLinkPath();

    $linkOptions = $this->alterOptions($linkOptions, $entity);

    $url = Url::fromUri($linkPath);
    $url->setOptions($linkOptions);
    $url->setOption('path', $linkPath);
    $link = Link::fromTextAndUrl($this->getLinkTitle(), $url);

    return $link;
  }

  /**
   * Get the provider name.
   *
   * @return string
   *   The provider name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Get link title.
   *
   * If SVG is set, render it and add it in place of the title.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|mixed
   *   Translatable title or SVG.
   */
  protected function getLinkTitle() {
    if ($svg = $this->getConfigValue('svg')) {
      $svg = [
        '#markup' => Markup::create(
          '<svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#' . $svg . '"></use></svg>'
        )
      ];
      return \Drupal\Core\Render\RendererInterface::render($svg);
    }

    return t(ucfirst($this->getName()));
  }

  /**
   * Build link path.
   *
   * Default link building behaviour.
   *
   * @return string
   */
  protected function buildLinkPath() {
    $request = \Drupal::request();
    $entityUrl = urlencode($request->getUri());
    $routeMatch = \Drupal::routeMatch();
    $pageTitle = urlencode(
      \Drupal::service('title_resolver')->getTitle($request, $routeMatch->getRouteObject())
    );

    return sprintf($this->getConfigValue('path'), $entityUrl, $pageTitle);
  }

  /**
   * Build link options.
   *
   * @return array
   *   Link options array.
   */
  protected function buildLinkOptions() {
    return [
      'path' => '',
      'html' => true,
      'attributes' => [
        'title' => t(ucfirst($this->getName())),
        'class' => [
          'social-link',
          $this->getName() . '-social-link',
        ],
      ],
    ];
  }

  /**
   * Base alter functionality.
   *
   * @param array $linkOptions
   *   Link options.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The given entity.
   *
   * @return array
   *   If the callback is callable.
   */
  public function alterOptions($linkOptions, EntityInterface $entity) {
    return $linkOptions;
  }

  /**
   * Get config value from array key.
   *
   * @param $key
   *   The key of the config.
   *
   * @return string|bool
   *   The associated value or false.
   */
  public function getConfigValue($key) {
    if (array_key_exists($key, $this->config) && !empty($this->config[$key])) {
      return $this->config[$key];
    }

    return FALSE;
  }
}

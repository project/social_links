<?php

namespace Drupal\social_links\Service;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\social_links\SocialLink;
use Drupal\social_links\SocialLinkInterface;

class SocialLinkFactory {

  /**
   * @var array
   */
  protected $links = [];

  /**
   * @var string
   */
  protected $themeFunction = 'item_list';

  /**
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  public function __construct(
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->logger = $loggerChannelFactory->get('social_links');

    $this->registerLinks($this->getDefaults());
  }

  /**
   * Register links and allow them to be altered by hooks.
   *
   * @param array $links
   *   Array of link config keyed by provider name.
   * @param bool $override
   *   If true, all existing links will be removed.
   */
  public function registerLinks(array $links, $override = FALSE) {
    if ($override == TRUE) {
      $this->links = [];
    }

    $hookLinks = $this->moduleHandler->invokeAll('social_links_alter', [$links]);

    if (!empty($hookLinks)) {
      $links = $hookLinks;
    }

    $this->addLinks($links);
  }

  /**
   * Add new links to existing links.
   *
   * @param array $links
   *   Array of links to be combined with existing links.
   */
  public function addLinks(array $links) {
    $this->links = array_merge($this->links, $links);
  }

  /**
   * Get render array of all links for a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to render the links for.
   * @param string $themeOverride
   *   Theme function to use instead of the default 'item_list'.
   *
   * @return array
   *   Render array.
   */
  public function renderLinks(EntityInterface $entity, $themeOverride = FALSE) {
    return [
      '#items' => $this->getRenderItems($entity),
      '#theme' => $this->getThemeFunction($themeOverride),
      '#attributes' => [
        'class' => [
          'social-links',
        ],
      ],
      '#attached' => [
        'library' =>  [
          'social_links/social_links'
        ],
      ],
    ];
  }

  /**
   * Get a render array of links for a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to add the links to.
   *
   * @return array
   *   Render array.
   */
  public function getRenderItems(EntityInterface $entity) {
    $renderArray = [];

    $links = $this->getLinks();

    foreach ($links as $provider => $config) {
      try {
        $class = $this->loadClass($config);
        $socialLink = new $class($provider, $config);
        $renderArray[] = $socialLink->render($entity);
      } catch (\Exception $exception) {
        $this->logger->error($exception->getMessage());
      }
    }

    return $renderArray;
  }

  /**
   * Get all registered links.
   *
   * @return array
   *   Raw array of link config keyed by provider name.
   */
  public function getLinks() {
    return $this->links;
  }

  /**
   * Get theme function.
   *
   * @param string|boolean $override
   *   Theme function to use instead of the default 'item_list'
   *
   * @return string
   *   The theme function name.
   */
  public function getThemeFunction($override = FALSE) {
    if ($override == FALSE) {
      return $this->themeFunction;
    }

    $this->themeFunction = $override;

    return $override;
  }

  /**
   * Check FQCN for a provider, or use default.
   *
   * @param array $config
   *  Config array.
   *
   * @return string
   *   FQCN string for the provider to use.
   */
  protected function loadClass(array $config) {
    if (array_key_exists('class', $config) == FALSE) {
      return SocialLink::class;
    }

    if (class_exists($config['class'])) {
      return $config['class'];
    }

    return SocialLink::class;
  }

  /**
   * Default social links.
   *
   * @return array
   *   Array of default links.
   */
  public function getDefaults() {
    return [
      'twitter' => [
        'path' => 'https://twitter.com/intent/tweet?url=%s&text=%s',
        'class' => '\Drupal\social_links\SocialLinkPopUp',
      ],
      'facebook' => [
        'path' => 'https://www.facebook.com/sharer/sharer.php?u=%s&quote=%s',
        'class' => '\Drupal\social_links\SocialLinkPopUp',
      ],
      'email' => [
        'path' => 'mailto:?body=%s&subject=%s',
      ],
    ];
  }
}

<?php

/**
 * @file
 * API documentation for Social Links.
 */

/**
 * Define social link providers and alter defaults.
 *
 * @return array
 */
function hook_social_links_alter($links) {
  // Add a linkedin provider, using the pop up class.
  $links['linkedin'] = [
    'path' => 'https://www.linkedin.com/sharing/share-offsite/?url=%s&title=%s',
    'class' => '\Drupal\social_links\SocialLinkPopUp',
  ];

  return $links;
}
